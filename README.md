# IAMU Documentation

## Run documentation local

```
cd website
yarn
yarn start
```

## Build documentation

```
yarn build
```

The build directory will be created in the "website" folder.

## Edit documentation

- create a .md file in "docs" directory with following header:

```
---
id: doc2
title: document number 2
---
```

- create a new entry in the "sitebars.json" inside of the website directory

For more information see the Docusaurus [documentation](https://docusaurus.io/docs/en/next/site-preparation).
