---
id: tags
title: Tag
sidebar_label: Tag
---

Der IAMU TAG ist eine Chip, welcher am Körper oder Objekt angebracht wird das geortet werden soll.  
Er sendet einen „Blink“ in einer variablen Rate.  
Ein „Blink“ ist ein Datensatz, der den aktuellen Zeitpunkt an die Anchor übersendet, die sich in einem Umkreis von 30 Meter zum TAG befinden.
Es gibt den TAG in diversen Ausführungen, um für verschiedene Anwendungsbereiche die beste Lösung zu realisieren. Dabei werden die Akkukapazität, verschiedene Sensoren zur Erweiterung des Trackingsystems und das Design individuell an Ihre Bedürfnisse angepasst.  
*Weitere Informationen hierzu finden Sie in den nachfolgenden Anwendungsbereichen.*

![IAMU Tag](assets/tags.png "IAMU Tag")