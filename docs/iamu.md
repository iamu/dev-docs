---
id: iamu
title: Allgemeine Informationen
sidebar_label: Was ist IAMU?
---
# Was ist IAMU?

**IAMU ist ein Echtzeit Personentracking System. Es bietet die Möglichkeit Personen, Fahrzeuge und Objekte in Echtzeit zu lokalisieren. Dabei verwendet IAMU eine Kombination aus Sendern (nachfolgend „TAG“ genannt) und den dazugehörigen Empfängern (nachfolgend „Anchor“ genannt). Diese kommunizieren über das UWB (Ultra-Breitband-Technologie, engl.: Ultra-wideband) Nahbereichsfunkkommunikation Netz.**

## Die UWB Technologie 

Die UWB Technologie nutzt einen besonders großer Frequenzbereiche von 500MHz und wird auch von Systemen wie dem Bodenradar, in der Medizintechnik für das Monitoring von Herzdaten und zum Übersenden von hochratigen Daten über kurze Entfernungen eingesetzt.


