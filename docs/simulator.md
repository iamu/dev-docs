---
id: simulator
title: Simulator
sidebar_label: Simulator
---

## Der IAMU Simulator
Zur Vorbereitung der Exponate stellt IAMU einen Simulator bereit. Dieser ist unter [www.iamu.dev/simulator](https://iamu.dev/simulator) erreichbar.  \
Der Simulator bietet die Möglichkeit einen oder mehrere TAGs zu simulieren, welche sich im Raum und der Zone bewegt.  