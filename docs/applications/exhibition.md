---
id: exhibition
title: In einer Ausstellung
sidebar_label: In einer Ausstellung
---

IAMU kann Ihnen dabei helfen, Ihre Ausstellung noch attraktiver und individueller zu
machen, indem Sie Nutzeranalysen oder Auswertungen des Besucherflusses auswerten.
Auch bei der Interaktion mit Exponaten ist Ihnen IAMU behilflich. So können interaktive
und exklusive Exponate erstellt werden, welche auf jeden Besuch einzeln reagieren. Ein
Beispiel hierfür ist, dass ein Besucher an ein Exponat heran tritt und über eine visuelle
Darstellung, oder akustische Wiedergabe, Informationen zum Exponat erhält. Hierbei
kann eine Ausgabe in der Landessprache des Besuchers geschehen, um Ihre
Ausstellung für alle Besucher inklusiv zu machen. Somit werden jedem Besucher die
Informationen zugänglich gemacht, während diese sich entscheiden können, was Sie
wirklich interessiert und an welcher Stelle Sie gerne erweiterte Informationen zu
bekommen möchten. Zudem erhalten Sie Informationen darüber, welche Exponate für
die Besucher besonders interessant sind und können somit Ihre Ausstellung immer an
die Interessen der Besucher anpassen.
Auch eine Vielzahl von anderen Interaktionsmöglichkeiten wäre denkbar.