---
id: examples
title: Anwendungsbeispiele
sidebar_label: Anwendungsbeispiele
---

## BrightSign 
### (Ton- , Videowiedergabe & Text Visualisierung)
Über den BrightSign kann eine Ton- oder Videowiedergabe als Interaktion beim Betreten
einer vordefinierten Zone erfolgen. Diese können mit verschiedenen Parametern, z.B. die
ausgewählte Sprache des Benutzers, versehen werden, um ein angepasstes Medium
auf einem Bildschirm oder Lautsprecher wieder zu geben. Darüber hinaus ist auch eine
Visualisierung mit verschiedenen Texten möglich. Auch diese werden auf einem
Bildschirm dargestellt.
## WebApp 
### (Ton- , Videowiedergabe & Text Visualisierung)
Mit Hilfe der WebApp von IAMU können sowohl Eingabe Oberflächen bereitstellen, als
auch Ausgaben von gesammelten Informationen, sowie Steuerung von externen System
(z.B. die Ansteuerung von Lichttechnischen Einrichtungen über das DMX 512-A
Protokoll) realisiert werden.
Darüber hinaus können auch MicroController mit Steuerbefehlen ausgestattet werden,
um eine Mechanische Bewegungen auszuführen.
## Raspberry Pi 
### (Licht- ,Ton- , Kamera- , Sensoren- & Mechanik Steuerung)
Über den Raspberry Pi Single-board Computer werden Steuerbefehle verarbeitet,
welche zum Beispiel an Mechanische Komponenten weiter gegeben werden können.
Auch Sensoren jeglicher Art können von einem Raspberry Pi angesteuert werden, um
Daten für den Benutzer bereit zu stellen. Dazu zählt zum Beispiel ein Infrarot Sensor,
welcher das Durchbrechen einer Lichtschranke erkennt.
Über den CSI Port besteht zudem die Möglichkeit, eine Kamera zu verbinden, um den
Benutzer des IAMU Personentracking Systems ein Foto von sich, oder der Umgebung,
bereitstellen zu können.
Darüber hinaus können über den DSI Port Touchscreen Displays angesteuert werden,
womit sich Benutzereingaben verarbeiten lassen.