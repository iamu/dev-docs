---
id: applications
title: Anwendungsbereiche
sidebar_label: Anwendungsbereiche
---

Das Personentrackingsystem von IAMU findet in diversen Bereichen seine Anwendung. In den
folgenden 4 Kapiteln werden Ihnen verschiedene Anwendungen beispielhaft vorgestellt. \
*Weitere Verwendungszwecke sind selbstverständlich auch möglich.*
