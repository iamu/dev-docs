---
id: schnittstellen
title: document number 2
sidebar_label: Test1234
---

# Schnittstellen

## HTTP Server Sent Events

Bei Nutzung dieser Web Technologie sendet der Server nach Aufbau der Verbindung Events, die eine Angabe zum Typ und einen JSON-serialisierten Payload enthalten. Diese Schnittstelle ist besonders für Browser-basierte Anwendungen geeignet, da Browser eine native Integration mit dieser Schnittstelle (EventSource) besitzen. Auch werden bei dieser Methode wenig Daten verbraucht und die Batterie des empfangenden Gerätes geschont.
