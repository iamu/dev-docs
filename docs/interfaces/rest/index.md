---
id: index
title: REST API
sidebar_label: Allgemein
---

## Allgmeine Informationen

Die unterschiedlichen Entitäten, die in IAMU eine Rolle spielen, sind per REST API verfügbar.
Listenansichten unterstützen dabei Filterung, Paginierung und Sortierung. Durch diese 3 Features lassen sich eine vielzahl von Usecases abbilden und ermöglichen Ihnen einfach die von Ihnen angeforderten Daten zu bekommen.

## Authentifizierung

Die REST API nutzt die gleichen Mechanismen zur Authentifizierung, wie die SSE API. Das bedeutet:

Jeder API Request muss einen `x-api-key` header beinhalten, der Ihren API Key enthält.

## Filterung

Einige Listenansichten lassen sich individuell filtern. Eine Beschreibung der Filtermöglichkeiten finden Sie in den jeweiligen Endpunkten.

## Paginierung

Endpunkte, die Listen von Entitäten zurückliefern, begrenzen die Anzahl der zurückgelieferten Werte. Auf diesem Weg ist es möglich sowohl eine geringe Latenz zu garantieren, als auch die Möglichkeit zu bieten alle Entitäten zu listen.

JSON-Antworten mit Paginierung sehen im IAMU-System wie im folgenden Beispiel aus:

```json
{
  "count": 25,
  "offset":0,
  "page_size":20,
  "data":[ {entity_1}, {entity_2}, ... ]}
```

Die eigentlich serialisierten Entitäten befinden sich im key `data` als Array. Es werden maximal `page_size` viele Entitäten ausgeliefert.

Falls nach einer Anfrage weitere Entitäten benötigt werden, muss ein weiterer Request an den jeweiligen Endpunkt gesendet werden. Hierbei ist der HTTP-Query-Parameter `offset` um den Wert von `page_size` zu erhöhen.

Z.b. `GET /users?page-size=50&offset=100` liefert die Ergebnisse von 100-149, für den Fall, so viele Benutzer angelegt wurden.

## Sortierung

Die Listenansichten lassen sich außerdem sortieren. Dazu übergeben Sie per Queryparameter den Parameter `sort_by` und geben an, wie Sie die Liste sortieren wollen. Dabei können Sie mehrere Sortierungskriterien spezifizieren und pro Kriterium ob auf- oder absteigend sortiert werden soll.

Um ein Kriterium anzugeben geben Sie zunächst ein Attribut der Entität an. Möchten Sie die Sortierungsrichtung angeben, geben Sie danach `.desc` oder `.asc` an.

Beispiele:

```
/tags?sort_by=mac # sortierung nach mac aufsteigend (implizit)
/tags?sort_by=mac.asc # sortierung nach mac aufsteigend (explizit)
/tags?sort_by=mac.desc # sortierung nach mac absteigend
```

Mehrere Sortierungskriterien können mit Komma getrennt angegeben werden:

`/tags?sort_by=mac.desc,nfc_id.asc`
