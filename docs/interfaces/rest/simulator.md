---
id: simulator
title: Simulator API
sidebar_label: Simulator API
---

# Simulator API

Die Simulator API ist der direkte Zugang zu simulierten Tags.

## Tags

Unter `/tags` wird die Funktionalität der [Identity API](identity#tags) gespiegelt und es wird empfohlen für simulierte Tags stets die Simulator API zu nutzen.

Der Simulator verwaltet die Tags ausschliesslich via ihrer simulierten Mac Adresse. Das bedeutet, dass die URLs ein wenig anders sind:

| Identity API           | Simulator API      |
| ---------------------- | ------------------ |
| /tags/mac/aaaaaaaaaaaa | /tags/aaaaaaaaaaaa |

Zusätzlich erlaubt es die Simulator API die Position simulierter Tags zu ändern:

```
PATCH /tags/aaaaaaaaa

{"x": 20, "y": 14.2}
```
