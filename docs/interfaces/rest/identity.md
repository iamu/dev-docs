---
id: identity
title: Identity API
sidebar_label: Identity API
---

# Identity API

Über die Identity API können User, Tags und Results verwaltet werden.

## Users

Unter `http://identity.$ihre-ausstellung.iamu.cloud/users` können Nutzer verwaltet werden:

Erlaubte Operationen sind: `GET /, GET /{id}, POST /, DELETE /{id}, PATCH /{id}, PUT /{id}`

Eine einzelne User-Entity, sieht wie folgt aus:

```
{
  "id": "d1f620b3-fe1d-4ae3-a59d-c47f29e89920",
  "metadata": {
    "color": "orange",
    "language": "de",
    "name": "MOP"
  },
  "created_at": "2019-12-10T20:14:34.993745Z",
  "updated_at": "2019-12-10T20:14:34.993745Z"
}
```

Bitte beachten Sie, dass bei den Metadaten Werte und Schlüssel aktuell nur vom Typ `String` sein dürfen.

Die Metadaten werden in den SSE Streams mit an den Benutzer gehangen und sollten mit allen Exponatbauern abgestimmt werden.

Ein `GET` auf `/users` liefert eine paginierte Liste - Siehe [Paginierung](index#Paginierung)

### Filterung

Die Userlist lässt sich wie folgt filtern:

`GET /users?start=2020-01-01`

Liefert User, die ab dem 01.01.2020 erstellt wurden.

`GET /users?end=2020-01-01`

Liefert User, die bis zum 01.01.2020 erstellt wurden.

### Results

Unter der User Unterroute `/results` können die Ergebnisse für einen Benutzers erstellt oder abgerufen werden.

Beispiel-URL für den obigen User:

`/users/d1f620b3-fe1d-4ae3-a59d-c47f29e89920/results`

Erlaubte Operationen sind: `GET /results, POST /results`

Beim Erstellen von Results per POST, muss das Request-JSON den Schlüssel `exhibit_id` enthalten. Z.B.:

```json
{
  "exhibit_id": "my-device",
  "result": {
    "vote": 1
  }
}
```

Per GET auf /results kann eine Auswertung die Liste aller Resultate für alle Ausstellungsstücke erhalten. Dieser Endpunkt liefert die Ergebnisse paginiert - siehe [Paginierung](index#Paginierung).

Für einzelne Exponate wird jedoch empfohlen, die Unterroute `/results/{exhibit_id}` zu verwenden, um ein Resultat abzuspeichern.

Hier sind folgende Operationen erlaubt: `GET /results/{exhibit_id}, PUT /results/{exhibit_id}`, PATCH `/results/{exhibit_id}` und DELETE `/results/{exhibit_id}`.

Die `exhibit_id` können Sie selbst wählen und muss alphanumerisch sein. Bindestriche sind ebenfalls erlaubt.

Eine Result-Entity sieht wie folgt aus:

```json
{
  "user_id":"fc2ef826-a797-41b9-9c69-cf717308d923",
  "exhibit_id":"test",
  "result": {
    ...
  },
  "created_at":"2020-03-16T13:53:13.704544321Z"
}
```

Unter dem Key `result` können Exponate beliebige als JSON-Objekt darstellbare Daten abspeichern. Bei der Verwendung der HTTP-Funktion Patch werden diese mit vorhandenen Daten kombiniert, wobei neuere Werte ältere überschreiben, alte Werte aber nicht verloren gehen.

## Tags

Unter `http://identity.$ihre-ausstellung.iamu.cloud/tags` können Tags verwaltet werden.

Ein Tag kann auf verschiedene Arten angefragt werden. Entweder nach seiner `mac`, seiner `nfc` oder dem aktuell zugeordneten User (`uuid`).

Erlaubte Operationen sind: `GET /, GET /mac|nfc|uuid/{id}, POST /, DELETE /mac|nfc|uuid/{id}, PATCH /mac|nfc|uuid/{id}, POST /mac|nfc|uuid/{id}/reset`

Eine Tag-Entity sieht wie folgt aus:

```
{
  "mac": "22025cf7a7ea",
  "nfc_id": "2237400200",
  "alternative_nfc_id": "1986462856",
  "simulated": true,
  "x": 20,
  "y": 14.2,
  "z": 0,
  "user_id": "0fd18b26-c636-4c07-981c-f9d81b68ac63"
},
```

`user_id` entspricht dabei dem aktuell zugeordneten Nutzer des Tags, wie z.B. von der Login-Station vergeben. Ist kein benutzer zugeordnet, enthält es den Wert `null`.

Die `nfc_id` referenziert die physikalische Adresse des NFC-Modules auf dem Tag. Da einige Exemplare zwei NFC-Module haben, kann eine weiteres NFC-Modul optional in dem Feld `alternative_nfc_id` gespeichert werden.

Das `simulated` Attribut bestimmt, ob ein Tag ein reales Tag ist, oder über den Simulator simuliert werden soll. Bitte beachten Sie, dass simulierte Tags idealerweise über die [Simulator API](simulator) verwaltet werden, da sich Änderungen ansonsten nicht unmittelbar auswirken.

Ein POST Request `/{id}/reset` löst die Zuordnung zu dem Benutzer, so dass anschließend das Tag neuen Benutzern zugeordnet werden kann.

Ein `GET`-Request auf `/tags/ liefert die Tags paginiert. Siehe [Paginierung](index#Paginierung)

Bitte beachten Sie, dass die Positionsdaten readonly sind und keine Echtzeitinformationen darstellen.

### Filterung der Tagliste

Die Tagliste bietet folgende Filterungsmöglichkeiten:

`GET /tags?simulated=(0|1)`

Hierdurch lässt sich die Liste einschränken auf "nur simulierte" bzw. "alle echten" Tags.
