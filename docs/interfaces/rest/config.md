---
id: config
title: Config API
sidebar_label: Config API
---

# Config API

Über die Config API können Zonen und der Raum verwaltet werden.

## Zonen

Unter `http://config.$ihre-ausstellung.iamu.cloud/zones` können Sie Zonen verwaltet werden.

Erlaubte Operationen sind: `GET /, GET /{id}, POST /, DELETE /{id}, PUT /{id}

Eine einzelne Zone wird im IAMU-System wie folgt serialisiert:

```json
{
  "id": "25956d84-3817-4fa2-9443-8de3452816b1",
  "name": "test",
  "polygon": [
    {
      "x": 0,
      "y": 0
    },
    {
      "x": 10,
      "y": 0
    },
    {
      "x": 10,
      "y": 10
    },
    {
      "x": 0,
      "y": 10
    }
  ],
  "bounding_box": {
    "top": 0,
    "left": 0,
    "width": 10,
    "height": 10
  },
  "created_at": "2020-01-20T15:05:03.775237Z",
  "updated_at": "2020-01-20T15:05:03.775237Z"
}
```

Die `bounding_box` ist dabei readonly und wird on the fly aus dem Polygon berechnet.

Ein GET auf `/zones` liefer eine paginierte Liste. Siehe [Paginierung](index#Paginierung)

# Raum

IAMU unterstützt zur Zeit einen (optionalen) Raum. Dieser hat aktuell nur eine Bedeutung für die Anzeige im Simulator.

Erlaubte Operationen sind: `GET /, PATCH /, DELETE /`

Die Raumentity sieht wie folgt aus:

```
{
  "left": 0,
  "top": 0,
  "right": 10,
  "bottom": 15,
  "image": "data:image/png;base64,...
}
```

Das `image` ist eine sogenannte Data-URL und enthält das Bild base64 encoded: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs
