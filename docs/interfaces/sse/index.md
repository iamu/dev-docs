---
id: index
title: Events
sidebar_label: Allgemein
---

# Allgemeine Informationen

IAMU kann Ihnen kontinuierliche Updates zu Tags und Zonen schicken. Für den Transport nutzt IAMU sogenannte [Server Sent Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events). Es handelt sich um einen Mechanismus, bei dem ein Server Informationen per HTTP zu einem Klienten schickt.
Diese Kommunikation geschieht unidirektional. Die Events werden dabei durch den IAMU Server mit JSON serialisiert.

Server Sent Events sind weit verbreitet und Clientimplementierungen gibt es in allen gängigen Sprachen und Plattformen.

# Endpunkte und Authentifizierung

Zu ihrem IAMU Zugang sollten Sie einen Hostnamen und APIKey erhalten haben. Ansonsten kontaktieren Sie bitte Ihren Ansprechpartner zur Ausstellung.

IAMU bietet mehrere Endpunkte für den Abruf von Server Sent Events an. Je nach Endpunkt werden unterschiedliche Typen von Events verschickt, die für unterschiedliche Anwendungsfälle modelliert wurden. Sie dazu die untergeordneten Punkte.

Endpunkte werden durch eine URL definiert, die sich aus dem bereits erwähnten Hostnamen und einen relativen Pfad zusammensetzen. Z.B. `http://sse.ihre-ausstellung.iamu.cloud/tags`.

Um Missbrauch zu vermeiden, müssen Sie beim Verbindungsaufbau einen HTTP-Header mitsenden, der den APIKey enthält und Sie identifiziert.
Mit dem Kommandozeilentool `curl` koennen Sie dies wie folgt testen:

```sh
curl -i -H 'x-api-key: $ihr-token' http://sse.$ihre-ausstellung.iamu.cloud/tags
```

Falls die Konfiguration korrekt ist, sollten sie hier bereits Events sehen. Selbst, wenn derzeit keine Livedaten verfuegbar sind, sollten sie immer zumindest Events vom Typ `ping` sehen. Die anderen Events sind in den dazugehörigen Detaildokumentionen beschrieben.

## Ping Event

Das Ping Event findet in allen Endpunkte Anwendung und wird zu Beginn Streams und bei fehlender Aktivität geschickt. Es signalisiert eine intakte Verbindung und wird alle 30s gesendet. Sollte Ihr Client nach 30s kein Ping Event oder ein anderes Event erhalten haben, sollten Sie die Verbindung erneut aufbauen.

```
event: ping
data: {}
```

## Umsetzung im Browser

Leider ist es in der Implementierung von Browsern nicht möglich den von IAMU benötigten HTTP Header zu setzen.

Dazu gibt es derzeit zwei mögliche Lösungen:

1. Verwendung eines [EventSource-Polyfill](https://github.com/EventSource/eventsource)
2. Verwendung eines eigenen kleinen Backends als Proxy, der die Authentifizierung gegenüber IAMU übernimmt.

Das Polyfill reimplementiert die EventSource API mit anderen Browsertechnologien und erlaubt als Erweiterung zum Standard aber das Setzen von HTTP-Headern im Browser-Kontext.

Ein eigenes Backend bietet sich dann an, wenn ohnehin ausserhalb des Browserkontextes Zustand gespeichert wird oder Dritt-Dienste angebunden werden.
