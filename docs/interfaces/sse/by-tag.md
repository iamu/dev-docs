---
id: by-tag
title: Tags
sidebar_label: Für Tags
---

Für ein Tracking von IAMU-Tags wird der Endpunkt `http://sse.$ihre-ausstellung.iamu.cloud/tags/$tag-id` zur Verfügung gestellt. Über diesen kann die Bewegung eines einzelnen Tags innerhalb der Ausstellung verfolgt werden.

Ein Event dieses Endpunktes hat folgendes Format:

```
event: data
data: {
  "id": "043251011239",
  "position": {
    "x": 0.37,
    "y": 5.12,
    "z": 0,
    "confidence_radius": 0.9
  },
  "zone_names": [],
  "user": {
    "id": "9e29ec73-4c66-481a-86e0-e8308b7894e5",
    "tag_mac": "043251011239",
    "nfc_id": "5099112311",
    "metadata": {
      "color": "#F50057",
      "language": "es",
      "name": "Lydia Leonardo"
    }
  }
}
```

Das `event: data` wird gesendet, sobald sich das Tag in der Ausstellung bewegt. Es enthält die folgende Informationen:

1. ID des TAGs
2. Die Position des TAGs (Koordinatensystem der Ausstellung. i.d.R. 0-X, 0-Y in Metern)
3. Namen aller Zonen in denen sich das TAG befindet
4. Userdaten
   - UID
   - Tag MAC-Adresse
   - NFC ID
   - Metadaten. siehe [User Rest API](../rest/identity).
