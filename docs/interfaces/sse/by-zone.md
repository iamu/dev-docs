---
id: by-zone
title: Zone
sidebar_label: Für Zonen
---

# Zonen-Endpunkt

Möchte ein Exponat Bewegungen in seiner Nähe tracken oder sich nähernde Besucher identifizieren, bietet sich der Zonen Endpunkt an. Innerhalb von IAMU können Zonen definiert werden (siehe auch [Config-Server-API]()../rest/config)). Aber auch ein Administrator kann Ihnen für eine Ausstellung solche Zonen anlegen.

Für einfache Exponate kann eine Zone definiert werden, die dem Standort des Exponats entspricht. Dann kann das Exponat auf herannahende Benutzer mit Tag reagieren.

Diesen Endpunkt erreichen Sie unter `http://sse.$ihre-ausstellung.iamu.cloud/zones/$zone-name`.

Ein Event dieses Endpunktes hat folgendes Format:

```
event: data
data: {
  "tag": {
    "id": "043251011239",
    "position": {
      "x": 4.77941,
      "y": 6.1651,
      "z": 0,
      "confidence_radius": 0
    },
    "user": {
      "id": "9e29ec73-4c66-481a-86e0-e8308b7894e5",
      "tag_mac": "043251011239",
      "nfc_id": "5099112311",
      "metadata": {
        "color": "#F50057",
        "language": "es",
        "name": "Lydia Leonardo"
      }
    }
  },
  "type": "UPDATE",
  "name": "test"
}
```

Ein Event enthält folgende Daten:

1. Taginformationen
   1. ID des TAGs
   2. Die Position des TAGs (Koordinatensystem der Ausstellung. i.d.R. 0-X, 0-Y in Metern)
   3. Namen aller Zonen in denen sich das TAG befindet
   4. Userdaten
      - UID
      - Tag MAC-Adresse
      - NFC ID
      - Metadaten. siehe [User Rest API](../rest/identity).
2. Typ des Events
3. Name der Zone

Ein Event wird in folgenden Fällen generiert:

1. Ein Tag betritt die Zone (type: "ENTER")
2. Ein Tag verlässt die Zone (type: "LEAVE")
3. Ein Tag bewegt sich innerhalb der Zone (type: "UPDATE")

## Parameter

Da viele Exponate nicht sämtliche oben genannte Events erhalten wollen, sondern an bestimmten Einsatzzwecken interessiert sind, gibt es die Möglichkeit die Abfrage des Endpunktes zu parametrisieren. Folgende beliebig kombinierbaren Request-Parameter sind derzeit implementiert:

### Smoothing

IAMU reicht ENTER und LEAVE Events für Zonen standardmässig sofort an das Exponat weiter. Befinden sich Tags am Rande einer Zone, oder bewegt sich jemand am Rande einer Zone nur kurz durch die Zone kann es bei Exponatslogik, die sich allein auf ENTER und LEAVE Events stützt durch das viele Kommen und Gehen von Tags zu ungewünschten Effekten kommen. Beispielsweise könnte ein Exponant eine Liste der Namen ausgeben, die nun anfängt sehr unruhig zu werden.

Mit Hilfe der Parameter `enter-jitter-threshold-ms` und `leave-jitter-threshold-ms` kann dieser Effekt abgeschwächt werden.

Wird der Parameter `enter-jitter-threshold-ms` wird erst nach angegebener Zeit in Millisekunden ein ENTER-Event im SSE-Stream gesendet - und auch nur dann, wenn in der Zwischenzeit kein LEAVE-Event gemessen wurde.

Analog funktioniert `leave-jitter-threshold-ms`. Hier muss das Tag zuerst die Zone betreten haben. Erst nach angegebener Zeit wird ein LEAVE-Event erzeugt, wenn zwischenzeitlich kein erneutes ENTER-Event gemessen wurde.

Beispiel für eine URL mit Smoothing:
`http://sse.$ihre-ausstellung.iamu.cloud/zones/$zone-name?enter-jitter-threshold-ms=5000&leave-jitter-threshold-ms=10000`

Bitte beachten Sie: Falls dieser Parameter nicht die gewünschten Ergebnisse liefert, gibt es weitere Möglichkeiten auf räumliche Gegebenheiten zu reagieren. Insbesondere können Zonen in der Größe variiert werden und müssen nicht rechteckig angelegt werden.

### Limitierung von Updates

Derzeit sendet IAMU Update-Events so schnell wie möglich. Braucht das Exponat lediglich gelegentlich ein Update eines Benutzers in der jeweiligen Zone, so kann der Query-Parameter `max-update-frequency-ms` auf einen Wert in der Einheit ms gesetzt werden. Für den Wert `1000` wird nur noch einmal pro Sekunde ein UPDATE-Event für die Tag-Position gesendet.

### keine Updates

Durch ein Anhängen des Parameter `disable-updates=1` werden keine UPDATE-Events mehr gesendet. Dies ist insbesondere für solche Exponate eine Entlastung der Verarbeitung von Events, die lediglich sich nähernde oder sich entfernende Tags erkennen wollen. Die Verwendung spart außerdem Netzwerkbandbreite.
