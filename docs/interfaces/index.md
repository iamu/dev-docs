---
id: index
title: Allgemein
sidebar_label: Allgemein
---

IAMU bietet eine Vielzahl von Schnittstellen, welche individuell auf Ihre Bedürfnisse
ausgerichtet werden können. In den Nachfolgenden Abschnitten, haben wir beispielhaft
ein paar Anwendungsbeispiele für Sie vorbereitet.

---

Folgende Events werden vom System erkannt und über die Schnittstellen weitergegeben:

- Ein Tag verändert seine Position
- Ein Tag betritt / verlässt eine definierte Zone (frei wählbar durch den Ersteller des Exponats)
- Die Liste von Tags in einer Zone ändert sich (sortiert nach Nähe zum Exponat).

Zu diesen Events kann eine Subscription auf ein bestimmtes oder alle Objekte (Tags, Zonen) erfolgen.
