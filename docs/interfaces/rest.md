---
id: rest
title: REST-API
sidebar_label: REST-API
---

Zum Abruf der verschiedenen Metadaten, die zu Beginn der Ausstellung abgefragt oder bei der
Interaktion mit den Exponaten gesammelt werden gibt es verschiedene REST-APIs auf Basis
von HTTP.
