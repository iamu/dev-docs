---
id: anchors
title: Anchors
sidebar_label: Anchors
---


Der Anchor von IAMU ist ein Chip, welcher sich in einem Gehäuse befindet um den Chip vor äußerlichen Einwirkungen wie der Temperatur, Staub und Sonneneinstrahlung zu schützt. Er wird über den zu ortenden Objekten, bevorzugt an der Decke, angebracht, um möglichst wenige Störobjekte zwischen sich und dem TAG zu haben. Dabei ist eine Entfernung von mindestens 15cm zu Decken und Wänden einzuhalten, welche über die speziell angefertigte Wand- und Deckenhalterung von IAMU gewährleistet wird.

Zur Synchronisierung mit den anderen Anchorn, wird von einem Master-Anchor die koordinierte Weltzeit UTC (engl.: Coordinated Universal Time) in einer getakteten Rate übersendet. Dieser Vorgang wird als „Sync“ bezeichnet.

Die Kommunikation der Anchor zum lokalen Server kann über eine Wi-Fi Verbindung oder eine direkte Ethernet-Verbindung geschaffen werden. Eine Ethernet-Verbindung wird hierbei empfohlen, da diese nicht anfällig für Störfrequenzen oder äußere Einflüsse ist, im Gegensatz zu einer Drahtlosen Wi-Fi Verbindung. Zudem kann im Falle eine Ethernet-Verbindung die Verstromung der Anchor über das Netzwerkkabel mit PoE (engl.: Power over Ethernet) erfolgen. Hierzu wird ein einsprechender Switch benötigt. \
*Im Falle einer Drahtlosen Verbindung muss eine zusätzliche Verstromung der Anchor erfolgen.*

![IAMU Tags](assets/vista.png "IAMU Tags")
![IAMU Tags](assets/vista2.png "IAMU Tags")