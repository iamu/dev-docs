# Inhaltsverzeichnis

## Was ist IAMU?

### Die UWB Technologie

---

## Tags

### Leonardo

### Piccolino

---

## Anchors

### Vista

---

## Anwendungsbereiche

### In der Industrie

### Im Sport

### In der Freizeit

### In einer Ausstellung

---

## Anwendungsbeispiele

## Schnittstellen

### Allgemein

### HTTP SSE

### Daten API`s für Besucher

#### User Daten

#### User Metadaten

#### User Results

#### Zonen Daten
